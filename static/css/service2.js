const data = [
    {
      id: 1,
      name: "browny daining table",
      img: "card1.jpg",
      amt: 7000,
      seller: "Namma Store",
      catagory: "Daining Table",
    },
  
    {
      id: 2,
      name: "best sofa",
      img: "card4.jpg",
      amt: 15000,
      seller: "Namma Store",
      catagory: "Sofa",
    },
  
    {
      id: 3,
      name: "daining table",
      img: "card8.webp",
      amt: 9000,
      seller: "Namma Store",
      catagory: "Daining Table",
    },
    {
      id: 4,
      name: "green chair",
      img: "card6.jpg",
      amt: 10070,
      seller: "Namma Store",
      catagory: "Chair",
    },
    {
      id: 5,
      name: "whitey DST",
      img: "d2.webp",
      amt: 13999,
      seller: "Namma Store",
      catagory: "Dreesing Table",
    },
  
    {
      id: 6,
      name: "sofa ",
      img: "card5.png",
      amt: 14999,
      seller: "Namma Store ",
      catagory: "Sofa",
    },
  
    {
      id: 7,
      name: "Dreesing table",
      img: "1.jpg",
      amt: 5009,
      seller: "Namma Store",
      catagory: "Dreesing Table",
    },
    {
      id: 8,
      name: "pinky chair",
      img: "pinky.webp",
      amt: 7000,
      seller: "Namma Store",
      catagory: "Chair",
    },
    {
        id: 9,
        name: "daining ",
        img: "t3.jpg",
        amt: 8000,
        seller: "Namma Store",
        catagory: "Daining Table",
      },
      {
        id: 10,
        name: "good dts",
        img: "d8.jpg",
        amt: 23000,
        seller: "Namma Store",
        catagory: "Dreesing Table",
      },
      {
        id: 11,
        name: "chair",
        img: "wooden.webp",
        amt: 9749,
        seller: "Namma Store",
        catagory: "Chair",
      },
      {
        id: 12,
        name: "sofa",
        img: "manasa.jpeg",
        amt: 24000,
        seller: "Namma Store",
        catagory: "Sofa",
      },
      {
        id: 13,
        name: "royal-dreesing table",
        img: "royal.webp",
        amt: 19049,
        seller: "Namma Store",
        catagory: "Dreesing Table",
      },
      {
        id: 14,
        name: "cheerypink sofa",
        img: "cheerypick sofa.jpg",
        amt: 20049,
        seller: "Namma Store",
        catagory: "Sofa",
      },
      {
        id: 15,
        name: "chair",
        img: "top chair.webp",
        amt: 10000,
        seller: "Namma Store",
        catagory: "Chair",
      },
     

   
  ];
  
  const productsContainer = document.querySelector(".products");
  const categoryList = document.querySelector(".category-list");
  
  function displayProducts(products) {
    if (products.length > 0) {
      const product_details = products
        .map(
          (product) => `
    <div class="product">
    <div class="img">
      <img src="${product.img}" alt="${product.name}" />
    </div>
    <div class="product-details">
      <span class="name">${product.name}</span>
      <span class="amt">Rs.${product.amt}</span>
      <span class="seller">${product.seller}</span>
    </div>
  </div>`
        )
        .join("");
  
      productsContainer.innerHTML = product_details;
    } else {
      productsContainer.innerHTML = "<h3>No Products Available</h3>";
    }
  }
  
  function setCategories() {
    const allCategories = data.map((product) => product.catagory);
    //console.log(allCategories);
    const catagories = [
      "All",
      ...allCategories.filter((product, index) => {
        return allCategories.indexOf(product) === index;
      }),
    ];
    //console.log(catagories);
    categoryList.innerHTML = catagories.map((catagory) => `<li>${catagory}</li>`).join("");
  
    categoryList.addEventListener("click", (e) => {
      const selectedCatagory = e.target.textContent;
      selectedCatagory === "All" ? displayProducts(data) : displayProducts(data.filter((product) => product.catagory == selectedCatagory));
    });
  }
  const priceRange = document.querySelector("#priceRange");
  const priceValue = document.querySelector(".priceValue");
  
  function setPrices() {
    const priceList = data.map((product) => product.amt);
    const minPrice = Math.min(...priceList);
    const maxPrice = Math.max(...priceList);
    priceRange.min = minPrice;
    priceRange.max = maxPrice;
    priceValue.textContent = "Rs." + maxPrice;
  
    priceRange.addEventListener("input", (e) => {
      priceValue.textContent = "Rs." + e.target.value;
      displayProducts(data.filter((product) => product.amt <= e.target.value));
    });
  }
  
  const txtSearch = document.querySelector("#txtSearch");
  txtSearch.addEventListener("keyup", (e) => {
    const value = e.target.value.toLowerCase().trim();
    if (value) {
      displayProducts(data.filter((product) => product.name.toLowerCase().indexOf(value) !== -1));
    } else {
      displayProducts(data);
    }
  });
  
  displayProducts(data);
  setCategories();
  setPrices();