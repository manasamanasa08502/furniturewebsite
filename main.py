import os
import random

from flask import Flask, request, render_template, url_for,flash,session, redirect
import smtplib
from email.mime.text import MIMEText
import mysql.connector
app = Flask(__name__)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"

app.secret_key="Manasa@12345"
UPLOAD_FOLDER = 'static/uploads/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
from werkzeug.utils import secure_filename
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def send_email(subject, body, sender, recipients, password):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(recipients)
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp_server:
       smtp_server.login(sender, password)
       smtp_server.sendmail(sender, recipients, msg.as_string())
    print("Message sent!")


@app.route("/forgotpassword")
def forgotpassword():
    return render_template("forgotpassword.html")

@app.route("/changepwd", methods=["POST","GET"])
def changepwd():
    try:
        pwd = request.form['pwd']
        email=session['email']
        sql="Update Customer set password = '%s' where emailid = '%s'" % (pwd, email)
        print("Sql : ", sql)
        conn = getConn()
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()
        msg="Password Changes Success"
        return render_template("signin.html", msg=msg)
    except Exception as e:
        return render_template("signin.html", msg=e)

@app.route("/checkotp", methods=["POST","GET"])
def checkotp():
    try:
        sentotp = request.form['otp']
        savedotp = session['otp']
        email=session['email']
        print("Saved Otp : ", savedotp, " Sent Otp : ", sentotp)
        if(int(sentotp)==int(savedotp)):
            return render_template("passwordchangepage.html", email = email)
        else:
            return render_template("enterotppage.html", email = email,msg='Incorrect OTP')
    except Exception as e:
        return render_template("signin.html", msg=e)

@app.route("/checkemail", methods=["POST","GET"])
def checkemail():
    try:
        email =request.form['email']
        sql="Select * from customer  where EmailId like '%s'" % (email)
        print("Sql : ", sql)
        conn = getConn()
        cursor = conn.cursor()
        cursor.execute(sql)
        row = cursor.fetchone()
        if(row):
            session['email']=email
            subject = "OTP to reset the password"
            otp = random.randint(1000,9999)
            session['otp']=otp
            body = "Thank you for changing the Password, your OTP is : " + str(otp)
            sender = "manasamanasa08502@gmail.com"
            recipients = [email]
            password = "nctvwaczkoltuzsf"
            send_email(subject, body, sender, recipients, password)
            return render_template("enterotppage.html", email = email)
        else:
            flash("Login Not Success")
            msg = 'Invalid EmailId'
            return render_template("forgotpassword.html", msg=msg)
    except Exception as e:
        return render_template("signin.html", msg=e)



def getConn():
    conn = mysql.connector.connect(host="localhost",
                                   user="root",
                                   password="",
                                   database="manasaproject")
    return conn
def executequery(sql):
    conn = getConn()
    cursor=conn.cursor()
    cursor.execute(sql)
    return cursor.fetchall()

def executeupdate(sql):
    conn = getConn()
    cursor=conn.cursor()
    cursor.execute(sql)
    conn.commit()

@app.route("/")
def final():
    return render_template("final.html")

@app.route("/adminsignin")
def adminsignin():
    return render_template("adminsignin.html")

@app.route("/checkadmin", methods=["POST","GET"])
def checkadmin():
    if(request.method=="POST"):
        uname = request.form['uname']
        pwd = request.form['pwd']
        if(uname=='admin' and pwd=='admin'):
            return render_template("mainpage.html")
    msg="Invalid UserName/Password"
    return render_template("adminsignin.html",msg=msg)




@app.route("/home")
def home():
    return render_template("home.html")

@app.route("/adminheader")
def adminheader():
    return render_template("header.html")

@app.route("/adminmainpage")
def adminmainpage():
    return render_template("mainpage.html")

@app.route("/service")
def services():
    return render_template("service.html")
@app.route("/collection")
def collection():
    return render_template("search.html")

@app.route("/about")
def about():
    return render_template("about.html")
@app.route("/location")
def location():
    return render_template("Location.html")

@app.route("/help")
def help():
    return render_template("furnitureend.html")

@app.route("/cart")
def cart():
    return render_template("cart.html")
@app.route("/Cart2")
def Cart2():
    return render_template("Cart2.html")
@app.route("/cart3")
def cart3():
    return render_template("cart3.html")
@app.route("/cart4")
def cart4():
    return render_template("cart4.html")
@app.route("/cart5")
def cart5():
    return render_template("cart5.html")
@app.route("/cart6")
def cart6():
    return render_template("cart6.html")

@app.route("/mainpage")
def mainpage():
    return render_template("mainpage.html")


@app.route("/success")
def success():
    return render_template("success.html")


@app.route("/newuser", methods=["POST","GET"])
def newuser():
    msg=''
    try:
        if request.method=="POST":
            print("Insert page")
            fname = request.form['fname']
            lname = request.form['lname']
            uname = request.form['uname']
            pwd = request.form['pwd']
            phnum = request.form['phnum']
            email = request.form['email']
            address = request.form['address']
            sql="Insert into customer(FirstName, LastName, LoginName, Password" \
                ",PhoneNum, Emailid, Address) " \
                "values('%s','%s','%s','%s','%s','%s','%s')" \
                %(fname,lname,uname,pwd,phnum,email,address)
            print("Sql : ",sql)
            conn = getConn()
            cursor = conn.cursor()
            cursor.execute(sql)
            conn.commit()
            msg = "Customer Registered Success"
    except Exception as e:
        #print(e.with_traceback())
        return render_template("register.html", msg=e)
    return render_template("register.html",msg=msg)

@app.route("/viewcustomers")
def viewcustomers():
    rows=[]
    cols=[]
    try:
        conn = getConn()
        sql="select * from customer"
        cursor= conn.cursor()
        cursor.execute(sql)
        rows=cursor.fetchall()
        sql="desc customer"
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()
        for x in data:
            cols.append(x[0])
    except Exception as e:
        return render_template("viewcustomers.html", msg=e)
    return render_template("viewcustomers.html", msg='', rows=rows, cols=cols)


@app.route("/deletecustomer", methods=["POST","GET"])
def deletecustomer():
    args=request.args
    id=args['id']
    sql="delete from customer where customerid="+str(id)
    print("sql : ", sql)
    conn=getConn()
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return redirect(url_for("viewcustomers"))

@app.route("/signin")
def signin():
    return render_template("signin.html")

@app.route("/addproduct", methods=["POST","GET"])
def addproduct():
    if request.method=="POST":
        print("request",request)
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No image selected for uploading')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            id = str(random.randint(1000, 9999))
            filename = "Img" + str(id) + ".jpg"
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            pname=request.form['pname']
            ptype = request.form['ptype']
            qty = request.form['qty']
            price = request.form['price']
            conn = getConn()
            cursor = conn.cursor()
            sql="Insert into Product(ProductName, ProductType, " \
                "Quantity, Price, Image) values('%s','%s','%s','%s','%s')" \
                %(pname, ptype, qty, price, filename)
            print("Sql : ", sql)
            cursor.execute(sql)
            conn.commit()
            cursor.close()
            conn.close()
            msg="Product Inserted Success"
            return render_template("addproduct.html", msg=msg)
        else:
            msg="Product Not Inserted Success"
            return render_template("addproduct.html", msg=msg)
    else:
        msg=''
        return render_template("addproduct.html",msg=msg)

@app.route("/addtocart", methods=["POST","GET"])
def addtocart():
    if(request.method=="POST"):

        pid=request.form['pid']
        pname=request.form['pname']
        ptype = request.form['ptype']
        qty = request.form['qty']
        price = request.form['price']
        rqty = request.form['rqty']
        total = request.form['total']
        cid=session['id']
        fname=session['fname']
        lname=session['lname']
        sql="Insert into CartTable(ProductId, ProductName, " \
            "ProductType, Quantity, Price, Total, " \
            "CustomerId, FirstName, LastName) values(%s,'%s','%s',%s,%s,%s,%s,'%s','%s')" \
            %(pid, pname, ptype, rqty, price, total, cid, fname, lname)
        print("Sql : ", sql)
        conn = getConn()
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()
        sql="Update Product set Quantity = Quantity - "+str(rqty)+" where ProductId = "+str(pid)
        print("Sql : ", sql)
        conn = getConn()
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        msg="Product Added to cart success"
        return redirect(url_for("customerviewcart"))
    else:
        msg=''
        return render_template("customerselectproduct.html",msg=msg)


@app.route("/viewproducts")
def viewproducts():
    rows=[]
    cols=[]
    try:
        conn = getConn()
        sql="select * from product"
        cursor= conn.cursor()
        cursor.execute(sql)
        rows=cursor.fetchall()

        sql="desc product"
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()

        for x in data:
            cols.append(x[0])
    except Exception as e:
        return render_template("viewproducts.html", msg=e)
    return render_template("viewproducts.html", msg='', rows=rows, cols=cols)

@app.route("/customerviewcart")
def customerviewcart():
    rows=[]
    cols=[]
    try:
        conn = getConn()
        id=session['id']
        sql="select * from carttable where customerid="+str(id)
        cursor= conn.cursor()
        cursor.execute(sql)
        rows=cursor.fetchall()

        sql="desc carttable"
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()

        for x in data:
            cols.append(x[0])
    except Exception as e:
        return render_template("customerviewcart.html", msg=e)
    return render_template("customerviewcart.html", msg='', rows=rows, cols=cols)


@app.route("/deleteproduct", methods=["POST","GET"])
def deleteproduct():
    args=request.args
    id=args['id']
    sql="delete from product where productid="+str(id)
    print("sql : ", sql)
    conn   =getConn()
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return redirect(url_for("viewproducts"))



@app.route("/bankdetail" ,methods=["POST","GET"])
def bankdetail():
    msg=''
    try:
        if request.method=="POST":
            print("Insert page")
            baname= request.form['bname']
            acno = request.form['accnum']
            icf = request.form['ifsccode']
            balance = request.form['balance']
            id=session['id']
            fname=session['fname']
            lname=session['lname']
            sql="Insert into banktable (BankName, AccountNumber, Ifsccode, " \
                "Balance, CustomerId, FirstName, LastName) " \
                "values('%s','%s','%s','%s','%s','%s','%s')" \
                %(baname,acno,icf,balance, id, fname, lname)
            print("Sql : ",sql)
            conn = getConn()
            cursor = conn.cursor()
            cursor.execute(sql)
            conn.commit()
            msg = "bankdetails updated"
    except Exception as e:
        return render_template("bank.html", msg=e)
    return render_template("bank.html",msg=msg)


@app.route("/viewbankdetail")
def viewbankdetail():
    rows=[]
    cols=[]
    try:
        conn = getConn()
        sql="select * from banktable"
        cursor= conn.cursor()
        cursor.execute(sql)
        rows=cursor.fetchall()

        sql="desc banktable"
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()

        for x in data:
            cols.append(x[0])
    except Exception as e:
        return render_template("viewbankdetail.html", msg=e)
    return render_template("viewbankdetail.html", msg='', rows=rows, cols=cols)

@app.route("/usermainpage")
def usermainpage():
    return  render_template("usermainpage.html")

@app.route("/customerselectproduct")
def customerselectproduct():
    args=request.args
    id=args['id']
    sql="select * from product where productid = " +str(id)
    conn = getConn()
    cursor = conn.cursor()
    cursor.execute(sql)
    data = cursor.fetchone()
    return  render_template("customerselectproduct.html", data=data)

@app.route("/customerviewproduct")
def customerviewproduct():
    return render_template("customerviewprodu")

@app.route("/adminlogincheck")
def adminlogincheck():
    try:
        uname = request.form['uname']
        pwd = request.form['pwd']
        if(uname=='admin' and pwd =='admin'):
            return render_template("mainpage.html")
        else:
            msg="Invalid UserName/Password"
            return render_template("adminsignin.html", msg=msg)
    except Exception as e:
        return render_template("adminsignin.html", msg=e)

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/userlogincheck",methods=["POST","GET"])
def userlogincheck():
      try:
        uname =request.form['uname']
        pwd = request.form['pwd']
        sql="Select * from customer  where LoginName like '%s' and Password Like '%s'" % (uname, pwd)
        print("Sql : ", sql)
        conn = getConn()
        cursor = conn.cursor()
        cursor.execute(sql)
        row = cursor.fetchone()

        if(row):
            flash("Login Successfully on" + str(uname))
            msg = 'Login Success'
            session['uname']=uname
            session['fname'] = row[1]
            session['lname'] = row[2]
            session['fullname'] = row[1]+ " " + row[2]
            session['id']=row[0]
            fullname =   row[1]+ " " + row[2]
            print("Full Name : ", fullname)
            return render_template("usermainpage.html", msg=msg, fullname = fullname)
        else:
            flash("Login Not Success")
            msg = 'Invalid UserName/Password'
            return render_template("signin.html", msg=msg)
      except Exception as e:
        return render_template("signin.html", msg=e)

@app.route("/userviewcustomer")
def userviewcustomer():
    rows=[]
    cols=[]
    try:
        conn = getConn()
        sql="select * from customer"
        cursor= conn.cursor()
        cursor.execute(sql)
        rows=cursor.fetchall()

        sql="desc customer"
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()

        for x in data:
            cols.append(x[0])
    except Exception as e:
        return render_template("userviewcustomer.html", msg=e)
    return render_template("userviewcustomer.html", msg='', rows=rows, cols=cols)

@app.route("/userdeletecustomer", methods=["POST","GET"])
def userdeletecustomer():
    args=request.args
    id=args['id']
    sql="delete from customer where customerid="+str(id)
    print("sql : ", sql)
    conn=getConn()
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return redirect(url_for("userviewcustomer"))
@app.route("/customerviewproducts")
def customerviewproducts():
    rows=[]
    cols=[]
    try:
        conn = getConn()
        sql="select * from product"
        cursor= conn.cursor()
        cursor.execute(sql)
        rows=cursor.fetchall()

        sql="desc product"
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()

        for x in data:
            cols.append(x[0])
    except Exception as e:
        return render_template("customerviewproducts.html", msg=e)
    return render_template("customerviewproducts.html", msg='', rows=rows, cols=cols)


@app.route("/customerdeleteproduct", methods=["POST","GET"])
def customerdeleteproduct():
    args=request.args
    id=args['id']
    sql="delete from product where productid="+str(id)
    print("sql : ", sql)
    conn=getConn()
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return redirect(url_for("customerviewproducts"))



@app.route("/logout")
def logout():
    session['uname']=''
    session['fullname'] = ''
    session['image'] = ''
    session.clear()
    return render_template("final.html")

if __name__ == '__main__':
    app.run(debug=True)
